﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulllet : MonoBehaviour
{
    private Rigidbody2D _rigidbody;

    public float speed;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        _rigidbody.velocity = transform.up * speed;
    }

    public void OnPlayerDamage()
    {
        Destroy(gameObject);
    }
}

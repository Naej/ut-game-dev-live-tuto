﻿using Cinemachine;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HeroController : MonoBehaviour
{
    private Rigidbody2D _rigidbody;

    public float speed;

    public int MaxHP = 3;
    private int currentHP;

    public Slider HPSlider;

    public Button GameOverButton;

    public GameObject deathCam;

    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        currentHP = MaxHP;
    }

    // Update is called once per frame
    void Update()
    {
        _rigidbody.velocity = new Vector2(Input.GetAxis("Horizontal"), 
            Input.GetAxis("Vertical")) * speed;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        Bulllet bullet = other.gameObject.GetComponentInParent<Bulllet>();

        if (bullet != null)
        {
            //prendre des dégâts
            currentHP--;
            HPSlider.value = currentHP * 1.0f / MaxHP;
            if (currentHP < 0)
            {
                gameObject.SetActive(false);
                GameOverButton.gameObject.SetActive(true);
                deathCam.SetActive(true);
            }
            
            //détruire le bullet
            bullet.OnPlayerDamage();
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}

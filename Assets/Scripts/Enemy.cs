﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public GameObject bullet;
    
    public float spawnPeriod = 1;
    public float rotationSpeed = 1;

    private float timeSinceLastBullet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(transform.forward,Time.deltaTime * rotationSpeed);
        
        timeSinceLastBullet += Time.deltaTime;
        if (timeSinceLastBullet > spawnPeriod)
        {
            //spawn bullet
            GameObject spawnedBullet = Instantiate(bullet);
            spawnedBullet.transform.position = transform.position;
            spawnedBullet.transform.up = transform.up;
            spawnedBullet.GetComponent<Bulllet>().speed = 10;

            timeSinceLastBullet = 0;
        }
    }
}
